﻿using Microsoft.AspNetCore.Mvc;
using op_soft.Datos;
using op_soft.Entidades.Almacen;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using op_soft.Web.Models.Almacen.Categorias;

namespace op_soft.Core.Servicios.Almacen
{
    [Route("api/[controller]")]
    [ApiController]
    class CategoriasServices: ICategoriasServices
    {
        //Declrar objeto
        private readonly DbContextSistema _context;

        //Constructor recibe parametro context
        public CategoriasServices(DbContextSistema context)
        {
            //Iniciar objeto context
            _context = context;
        }

        public async Task<IEnumerable<CategoriaViewModel>> GetCategorias()
        {
            //Obtener lista asincrona
            var categorias = await _context.Categorias.ToListAsync();
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return categorias.Select(c => new CategoriaViewModel
            {
                IdCategoria = c.IdCategoria,
                Nombre = c.Nombre,
                Descripcion = c.Descripcion,
                Estatus = c.Estatus
            });
        }

        Task<IEnumerable<Categorias>> ICategoriasServices.GetCategorias()
        {
            throw new NotImplementedException();
        }
    }
}
