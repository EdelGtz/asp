﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using op_soft.Entidades.Almacen;

namespace op_soft.Core.Servicios.Almacen
{
    public interface ICategoriasServices
    {
        Task<IEnumerable<Categorias>> GetCategorias();
    }
}
