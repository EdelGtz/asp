﻿using Microsoft.EntityFrameworkCore;
using op_soft.Datos.Mapping.Almacen;
using op_soft.Datos.Mapping.Compras;
using op_soft.Datos.Mapping.Usuarios;
using op_soft.Datos.Mapping.Ventas;
using op_soft.Entidades.Almacen;
using op_soft.Entidades.Compras;
using op_soft.Entidades.Usuarios;
using op_soft.Entidades.Ventas;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos
{
    public class DbContextSistema : DbContext
    {
        //Exponer colección de categorias
        public DbSet<Categorias> Categorias { get; set; }
        public DbSet<Articulos> Articulos { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<Proveedores> Proveedores { get; set; }
        public DbSet<ComprasModel> Compras { get; set; }
        public DbSet<Detalle_compras> Detalle_compras { get; set; }
        public DbSet<VentasModel> Ventas { get; set; }
        public DbSet<Detalle_ventas> Detalle_ventas { get; set; }

        public DbContextSistema(DbContextOptions<DbContextSistema> options) : base(options)
        {
            //optionsBuilder.UseSqlServer("Data Source=DESKTOP-KSA9CCV\\SQLEXPRESS;Initial Catalog=opdb;Integrated Security=True")
            //    .EnableSensitiveDataLogging(true)
            //    .UseLoggerFactory(new LoggerFactory().AddConsole((category, level) => level == LogLevel.Information && category == DbLoggerCategory.Database.Command.Name, true));
        }

        //Aplicar configucación
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CategoriaMap());
            modelBuilder.ApplyConfiguration(new ArticulosMap());
            modelBuilder.ApplyConfiguration(new RolMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new ClienteMap());
            modelBuilder.ApplyConfiguration(new ProveedorMap());
            modelBuilder.ApplyConfiguration(new ComprasMap());
            modelBuilder.ApplyConfiguration(new DetalleCompraMap());
            modelBuilder.ApplyConfiguration(new VentasMap());
            modelBuilder.ApplyConfiguration(new DetalleVentaMap());
        }
    }
}
