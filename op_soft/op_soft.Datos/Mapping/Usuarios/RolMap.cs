﻿using Microsoft.EntityFrameworkCore;
using op_soft.Entidades.Usuarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Usuarios
{
    public class RolMap : IEntityTypeConfiguration<Rol>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Rol> builder)
        {
            builder.ToTable("Roles")
                .HasKey(r => r.IdRol);
        }
    }
}
