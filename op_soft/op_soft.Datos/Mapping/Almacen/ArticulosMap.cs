﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Almacen;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Almacen
{
    public class ArticulosMap : IEntityTypeConfiguration<Articulos>
    {
        public void Configure(EntityTypeBuilder<Articulos> builder)
        {
            builder.ToTable("Articulos")
               .HasKey(a => a.IdArticulo);
            //builder.Property(a => a.Nombre)
            //    .HasMaxLength(100);
            //builder.Property(c => c.Descripcion)
            //    .HasMaxLength(250);
        }
    }
}
