﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Almacen;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Almacen
{
    public class CategoriaMap : IEntityTypeConfiguration<Categorias>
    {
        //Mapeo de la tabla Categorias
        public void Configure(EntityTypeBuilder<Categorias> builder)
        {
            builder.ToTable("Categorias")
               .HasKey(c => c.IdCategoria);
            builder.Property(c => c.Nombre)
                .HasMaxLength(50);
            builder.Property(c => c.Descripcion)
                .HasMaxLength(250);
        }
    }
}
