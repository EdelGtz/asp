﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Ventas;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Ventas
{
    public class ClienteMap : IEntityTypeConfiguration<Clientes>
    {
        void IEntityTypeConfiguration<Clientes>.Configure(EntityTypeBuilder<Clientes> builder)
        {
            builder.ToTable("Clientes")
                .HasKey(c => c.IdCliente);
        }
    }
}
