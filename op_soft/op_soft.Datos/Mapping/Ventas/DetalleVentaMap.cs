﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Ventas;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Ventas
{
    public class DetalleVentaMap : IEntityTypeConfiguration<Detalle_ventas>
    {
        void IEntityTypeConfiguration<Detalle_ventas>.Configure(EntityTypeBuilder<Detalle_ventas> builder)
        {
            builder.ToTable("Detalle_ventas")
                 .HasKey(d => d.IdDetalle_venta);
        }
    }
}
