﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Ventas;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Ventas
{
    public class VentasMap : IEntityTypeConfiguration<VentasModel>
    {
        void IEntityTypeConfiguration<VentasModel>.Configure(EntityTypeBuilder<VentasModel> builder)
        {
            builder.ToTable("Ventas")
                .HasKey(v => v.IdVenta);
            builder.HasOne(v => v.Cliente)
                .WithMany(p => p.Venta)
                .HasForeignKey(v => v.IdCliente);
        }
    }
}
