﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Compras;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Compras
{
    public class DetalleCompraMap : IEntityTypeConfiguration<Detalle_compras>
    {
        void IEntityTypeConfiguration<Detalle_compras>.Configure(EntityTypeBuilder<Detalle_compras> builder)
        {
            builder.ToTable("Detalle_compras")
                 .HasKey(d => d.IdDetalle_compra);
        }
    }
}
