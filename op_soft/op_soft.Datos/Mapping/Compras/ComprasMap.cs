﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Compras;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Compras
{
    public class ComprasMap : IEntityTypeConfiguration<ComprasModel>
    {
        void IEntityTypeConfiguration<ComprasModel>.Configure(EntityTypeBuilder<ComprasModel> builder)
        {
            builder.ToTable("Compras")
                .HasKey(c => c.IdCompra);
            builder.HasOne(c => c.Proveedor)
                .WithMany(p => p.Compras)
                .HasForeignKey(c => c.IdProveedor);
        }
    }
}
