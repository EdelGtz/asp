﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using op_soft.Entidades.Compras;
using System;
using System.Collections.Generic;
using System.Text;

namespace op_soft.Datos.Mapping.Compras
{
    public class ProveedorMap : IEntityTypeConfiguration<Proveedores>
    {
        void IEntityTypeConfiguration<Proveedores>.Configure(EntityTypeBuilder<Proveedores> builder)
        {
            builder.ToTable("Proveedores")
                .HasKey(c => c.IdProveedor);
        }
    }
}
