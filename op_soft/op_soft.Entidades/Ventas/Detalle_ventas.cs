﻿using op_soft.Entidades.Almacen;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Ventas
{
    public class Detalle_ventas
    {
        public int IdDetalle_venta { get; set; }
        [Required]
        public int IdVenta { get; set; }
        [Required]
        public int IdArticulo { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public decimal Precio { get; set; }
        [Required]
        public decimal Descuento { get; set; }

        public VentasModel Venta { get; set; }
        public Articulos Articulo { get; set; }

    }
}
