﻿using op_soft.Entidades.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Compras
{
    public class ComprasModel
    {
        public int IdCompra { get; set; }
        [Required]
        public int IdProveedor { get; set; }
        [Required]
        public int IdUsuario { get; set; }
        [Required]
        public string Tipo_comprobante { get; set; }
        public string Serie_comprobante { get; set; }
        [Required]
        public string Num_comprobante { get; set; }
        [Required]
        public DateTime Fecha_hora { get; set; }
        [Required]
        public decimal Impuesto { get; set; }
        [Required]
        public decimal Total { get; set; }
        [Required]
        public string Estado { get; set; }

        public ICollection<Detalle_compras> Detalle { get; set; }
        public Usuario Usuario { get; set; }
        public Proveedores Proveedor { get; set; }
    }
}
