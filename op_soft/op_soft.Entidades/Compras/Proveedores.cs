﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Compras
{
    public class Proveedores
    {
        public int IdProveedor { get; set; }
        public string CodigoProveedor { get; set; }
        [Required]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "El nombre no debe de tener más de 150 caracteres, ni menos de 3 caracteres.")]
        public string Razon_social { get; set; }
        [Required]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "El nombre no debe de tener más de 150 caracteres, ni menos de 3 caracteres.")]
        public string Nombre_comercial { get; set; }
        public string RFC { get; set; }
        public string Banco { get; set; }
        public string Numero_cuenta { get; set; }
        public string Tipo_documento { get; set; }
        public string Num_documento { get; set; }
        public int Dias_credito { get; set; }
        public int Monto_credito { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int Descuento { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }

        public ICollection<ComprasModel> Compras { get; set; }
    }
}
