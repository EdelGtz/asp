﻿using op_soft.Entidades.Almacen;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Compras
{
    public class Detalle_compras
    {
        public int IdDetalle_compra { get; set; }
        [Required]
        public int IdCompra { get; set; }
        [Required]
        public int IdArticulo { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public decimal Precio { get; set; }

        public ComprasModel Compras { get; set; }
        public Articulos Articulos { get; set; }
    }
}
