﻿using op_soft.Entidades.Compras;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Almacen
{
    public class Articulos
    {
        [Key]
        public int IdArticulo { get; set; }
        [Required]
        public int IdCategoria { get; set; }
        [Required]
        public string SKU { get; set; }
        public string CodigoSAT { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string Nombre { get; set; }
        public string NombreSAT { get; set; }
        public string Marca { get; set; }
        public string Presentacion { get; set; }
        public string UnidadSAT { get; set; }
        public decimal Precio_venta { get; set; }
        public int Stock { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }

        public Categorias Categorias { get; set; }
        public ICollection<Detalle_compras> Detalle_compras { get; set; }
    }
}
