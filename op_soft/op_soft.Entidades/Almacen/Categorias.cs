﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Almacen
{
    public class Categorias
    {
        [Key]
        public int IdCategoria { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Máximo 50 caracteres")]
        public string Nombre { get; set; }
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }

        public ICollection<Articulos> Articulos { get; set; }
    }
}
