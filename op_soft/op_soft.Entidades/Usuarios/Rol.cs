﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace op_soft.Entidades.Usuarios
{
    public class Rol
    {
        public int IdRol { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "El Nombre no debe tener más de 30 caracteres ni menos de 3")]
        public string Nombre { get; set; }
        [StringLength(100)]
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }
    }
}
