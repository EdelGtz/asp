﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using op_soft.Entidades.Compras;
using System.Text;
using op_soft.Entidades.Ventas;

namespace op_soft.Entidades.Usuarios
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        [Required]
        public int IdRol { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "El nombre no debe de tener más de 100 caracteres, ni menos de 3 caracteres.")]
        public string Nombre { get; set; }
        public string Tipo_documento { get; set; }
        public string Num_documento { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public byte[] Password_hash { get; set; }
        [Required]
        public byte[] Password_salt { get; set; }
        public bool Estatus { get; set; }

        public Rol Rol { get; set; }

        public ICollection<ComprasModel> Compras { get; set; }
        public ICollection<VentasModel> Venta { get; set; }
    }
}
