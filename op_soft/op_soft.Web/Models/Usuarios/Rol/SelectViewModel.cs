﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Usuarios.Rol
{
    public class SelectViewModel
    {
        public int IdRol { get; set; }
        public string Nombre { get; set; }
    }
}
