﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Usuarios.Usuario
{
    public class UsuarioViewModel
    {
        public int IdUsuario { get; set; }
        [Required]
        public int IdRol { get; set; }
        public string Rol { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "El nombre no debe de tener más de 100 caracteres, ni menos de 3 caracteres.")]
        public string Nombre { get; set; }
        public string Tipo_documento { get; set; }
        public string Num_documento { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public byte[] Password_hash { get; set; }
        public bool Estatus { get; set; }
    }
}
