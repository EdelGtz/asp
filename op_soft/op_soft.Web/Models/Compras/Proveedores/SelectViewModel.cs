﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Compras.Proveedores
{
    public class SelectViewModel
    {
        public int IdProveedor { get; set; }
        public string Nombre_comercial { get; set; }
    }
}
