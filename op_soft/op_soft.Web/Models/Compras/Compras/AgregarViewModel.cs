﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Compras.Compras
{
    public class AgregarViewModel
    {
        //Propiedades maestro
        [Required]
        public int IdProveedor { get; set; }
        [Required]
        public int IdUsuario { get; set; }
        [Required]
        public string Tipo_comprobante { get; set; }
        public string Serie_comprobante { get; set; }
        [Required]
        public string Num_comprobante { get; set; }
        [Required]
        public decimal Impuesto { get; set; }
        [Required]
        public decimal Total { get; set; }
        //Propiedades detalle
        [Required]
        public List<DetalleViewModel> Detalles { get; set; }
    }
}
