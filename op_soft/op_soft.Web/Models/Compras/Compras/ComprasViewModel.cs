﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Compras.Compras
{
    public class ComprasViewModel
    {
        public int IdCompra { get; set; }
        public int IdProveedor { get; set; }
        public string Proveedor { get; set; }
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
        public string Tipo_comprobante { get; set; }
        public string Serie_comprobante { get; set; }
        public string Num_comprobante { get; set; }
        public DateTime Fecha_hora { get; set; }
        public decimal Impuesto { get; set; }
        public decimal Total { get; set; }
        public string Estado { get; set; }
    }
}
