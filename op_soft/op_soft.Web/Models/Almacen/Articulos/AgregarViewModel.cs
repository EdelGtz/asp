﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Almacen.Articulos
{
    public class AgregarViewModel
    {
        [Required]
        public int IdCategoria { get; set; }
        public string SKU { get; set; }
        public string CodigoSAT { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres")]
        public string Nombre { get; set; }
        public string NombreSAT { get; set; }
        public string Marca { get; set; }
        public string Presentacion { get; set; }
        public string UnidadSAT { get; set; }
        public decimal Precio_venta { get; set; }
        public int Stock { get; set; }
        public string Descripcion { get; set; }
    }
}
