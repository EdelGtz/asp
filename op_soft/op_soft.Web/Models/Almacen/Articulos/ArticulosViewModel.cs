﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Almacen.Articulos
{
    public class ArticulosViewModel
    {
        public int IdArticulo { get; set; }
        public int IdCategoria { get; set; }
        public string Categoria { get; set; }
        public string SKU { get; set; }
        public string CodigoSAT { get; set; }
        public string Nombre { get; set; }
        public string NombreSAT { get; set; }
        public string Marca { get; set; }
        public string Presentacion { get; set; }
        public string UnidadSAT { get; set; }
        public decimal Precio_venta { get; set; }
        public int Stock { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }
    }
}
