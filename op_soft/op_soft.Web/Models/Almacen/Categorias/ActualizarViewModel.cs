﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Almacen.Categorias
{
    public class ActualizarViewModel
    {
        [Required]
        public int IdCategoria { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Máximo 50 caracteres")]
        public string Nombre { get; set; }
        [StringLength(250, ErrorMessage = "Máximo 250 caracteres")]
        public string Descripcion { get; set; }
    }
}
