﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace op_soft.Web.Models.Ventas.Clientes
{
    public class SelectViewModel
    {
        public int IdCliente{ get; set; }
        public string Nombre_comercial { get; set; }
    }
}
