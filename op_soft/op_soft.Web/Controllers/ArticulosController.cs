﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Almacen;
using op_soft.Web.Models.Almacen.Articulos;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticulosController : Controller
    {
        private readonly DbContextSistema _context;

        public ArticulosController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Articulos/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ArticulosViewModel>> Listar()
        {
            //Obtener lista asincrona
            var articulos = await _context.Articulos.Include(a => a.Categorias).ToListAsync();
            //Retornar el objeto instanciado de la clase ArticulosViewModel
            return articulos.Select(a => new ArticulosViewModel
            {
                IdArticulo = a.IdArticulo,
                IdCategoria = a.IdCategoria,
                Categoria = a.Categorias.Nombre,
                SKU = a.SKU,
                CodigoSAT = a.CodigoSAT,
                Nombre = a.Nombre,
                NombreSAT = a.NombreSAT,
                Marca = a.Marca,
                Presentacion = a.Presentacion,
                UnidadSAT = a.UnidadSAT,
                Precio_venta = a.Precio_venta,
                Stock = a.Stock,
                Descripcion = a.Descripcion,
                Estatus = a.Estatus
            });
        }

        // GET: api/Articulos/BuscarArticulo
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<ArticulosViewModel>> BuscarArticulo([FromRoute] string texto)
        {
            //Obtener lista asincrona
            var articulos = await _context.Articulos.Include(a => a.Categorias)
                .Where(a => a.Nombre.Contains(texto))
                .Where(a => a.Estatus == true)
                .ToListAsync();
            //Retornar el objeto instanciado de la clase ArticulosViewModel
            return articulos.Select(a => new ArticulosViewModel
            {
                IdArticulo = a.IdArticulo,
                IdCategoria = a.IdCategoria,
                Categoria = a.Categorias.Nombre,
                SKU = a.SKU,
                CodigoSAT = a.CodigoSAT,
                Nombre = a.Nombre,
                NombreSAT = a.NombreSAT,
                Marca = a.Marca,
                Presentacion = a.Presentacion,
                UnidadSAT = a.UnidadSAT,
                Precio_venta = a.Precio_venta,
                Stock = a.Stock,
                Descripcion = a.Descripcion,
                Estatus = a.Estatus
            });
        }

        // GET: api/Articulos/BuscarCodigoVenta/12345678
        [HttpGet("[action]/{codigo}")]
        public async Task<IActionResult> BuscarCodigoVenta([FromRoute] string codigo)
        {

            var articulo = await _context.Articulos.Include(a => a.Categorias)
                .Where(a => a.Estatus == true)
                .Where(a => a.Stock > 0)
                .SingleOrDefaultAsync(a => a.SKU == codigo);

            if (articulo == null)
            {
                return NotFound();
            }

            return Ok(new ArticulosViewModel
            {
                IdArticulo = articulo.IdArticulo,
                IdCategoria = articulo.IdCategoria,
                Categoria = articulo.Categorias.Nombre,
                SKU = articulo.SKU,
                Nombre = articulo.Nombre,
                Descripcion = articulo.Descripcion,
                Stock = articulo.Stock,
                Precio_venta = articulo.Precio_venta,
                Estatus = articulo.Estatus
            });
        }

        // GET: api/Articulos/ListarIngreso/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<ArticulosViewModel>> ListarVenta([FromRoute] string texto)
        {
            var articulo = await _context.Articulos.Include(a => a.Categorias)
                .Where(a => a.Nombre.Contains(texto))
                .Where(a => a.Estatus == true)
                .ToListAsync();

            return articulo.Select(a => new ArticulosViewModel
            {
                IdArticulo = a.IdArticulo,
                IdCategoria = a.IdCategoria,
                Categoria = a.Categorias.Nombre,
                SKU = a.SKU,
                Nombre = a.Nombre,
                Stock = a.Stock,
                Precio_venta = a.Precio_venta,
                Descripcion = a.Descripcion,
                Estatus = a.Estatus
            });

        }

        // GET: api/Articulos/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Buscar por ID
            var articulos = await _context.Articulos.Include(a => a.Categorias).SingleOrDefaultAsync(a => a.IdArticulo == id);

            if (articulos == null)
            {
                return NotFound();
            }
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return Ok(new ArticulosViewModel
            {
                IdArticulo = articulos.IdArticulo,
                IdCategoria = articulos.IdCategoria,
                Categoria = articulos.Categorias.Nombre,
                SKU = articulos.SKU,
                CodigoSAT = articulos.CodigoSAT,
                Nombre = articulos.Nombre,
                NombreSAT = articulos.NombreSAT,
                Marca = articulos.Marca,
                Presentacion = articulos.Presentacion,
                UnidadSAT = articulos.UnidadSAT,
                Precio_venta = articulos.Precio_venta,
                Stock = articulos.Stock,
                Descripcion = articulos.Descripcion,
                Estatus = articulos.Estatus
            });
        }

        // GET: api/Articulos/BuscarSKU/5
        [HttpGet("[action]/{sku}")]
        public async Task<IActionResult> BuscarSKU([FromRoute] string sku)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Buscar por ID
            var articulos = await _context.Articulos.Include(a => a.Categorias)
                .Where(a => a.Estatus == true)
                .SingleOrDefaultAsync(a => a.SKU == sku);

            if (articulos == null)
            {
                return NotFound();
            }
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return Ok(new ArticulosViewModel
            {
                IdArticulo = articulos.IdArticulo,
                IdCategoria = articulos.IdCategoria,
                Categoria = articulos.Categorias.Nombre,
                SKU = articulos.SKU,
                CodigoSAT = articulos.CodigoSAT,
                Nombre = articulos.Nombre,
                NombreSAT = articulos.NombreSAT,
                Marca = articulos.Marca,
                Presentacion = articulos.Presentacion,
                UnidadSAT = articulos.UnidadSAT,
                Precio_venta = articulos.Precio_venta,
                Stock = articulos.Stock,
                Descripcion = articulos.Descripcion,
                Estatus = articulos.Estatus
            });
        }

        // PUT: api/Articulos/Actualizar/5
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IdCategoria < 0)
            {
                return BadRequest();
            }
            //Consultar que la categoria exista en BD
            var articulos = await _context.Articulos.FirstOrDefaultAsync(a => a.IdArticulo == model.IdArticulo);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe la categoria
            if (articulos == null)
            {
                return NotFound();
            }
            //Si la categoria existe pasarle los valores al objeto
            articulos.IdCategoria = model.IdCategoria;
            articulos.SKU = model.SKU;
            articulos.CodigoSAT = model.CodigoSAT;
            articulos.Nombre = model.Nombre;
            articulos.NombreSAT = model.NombreSAT;
            articulos.Marca = model.Marca;
            articulos.Presentacion = model.Presentacion;
            articulos.UnidadSAT = model.UnidadSAT;
            articulos.Precio_venta = model.Precio_venta;
            articulos.Stock = model.Stock;
            articulos.Descripcion = model.Descripcion;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // POST: api/Articulos/Agregar
        [HttpPost("[action]")]
        public async Task<IActionResult> Agregar([FromBody] AgregarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Crear instancia de Categorias y pasarle los del model al objeto
            Articulos articulos = new Articulos
            {
                IdCategoria = model.IdCategoria,
                SKU = model.SKU,
                CodigoSAT = model.CodigoSAT,
                Nombre = model.Nombre,
                NombreSAT = model.NombreSAT,
                Marca = model.Marca,
                Presentacion = model.Presentacion,
                UnidadSAT = model.UnidadSAT,
                Precio_venta = model.Precio_venta,
                Stock = model.Stock,
                Descripcion = model.Descripcion,
                Estatus = true
            };
            //Agregar articulo desde el context
            _context.Articulos.Add(articulos);

            try
            {
                //Guardar cambios
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Articulos/Desactivar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que la categoria exista en BD
            var articulos = await _context.Articulos.FirstOrDefaultAsync(a => a.IdArticulo == id);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe la categoria
            if (articulos == null)
            {
                return NotFound();
            }
            //Si la categoria existe pasarle los valores al objeto
            articulos.Estatus = false;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // PUT: api/Articulos/Activar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que la categoria exista en BD
            var articulos = await _context.Articulos.FirstOrDefaultAsync(a => a.IdArticulo == id);
            
            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe la categoria
            if (articulos == null)
            {
                return NotFound();
            }
            //Si la categoria existe pasarle los valores al objeto
            articulos.Estatus = true;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        private bool ArticulosExists(int id)
        {
            return _context.Articulos.Any(e => e.IdArticulo == id);
        }
    }
}
