﻿using System.Threading.Tasks;

namespace op_soft.Web.Controllers
{
    public interface ICategoriasServices
    {
        Task GetCategorias();
    }
}