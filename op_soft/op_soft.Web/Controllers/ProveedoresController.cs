﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Compras;
using op_soft.Web.Models.Compras.Proveedores;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProveedoresController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ProveedoresController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Proveedores
        [HttpGet]
        public IEnumerable<Proveedores> GetProveedores()
        {
            return _context.Proveedores;
        }

        // GET: api/Proveedores/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ProveedorViewModel>> Listar()
        {
            //Obtener lista asincrona
            var proveedores = await _context.Proveedores.ToListAsync();
            //Retornar el objeto instanciado de la clase ProveedorViewModel
            return proveedores.Select(p => new ProveedorViewModel
            {
                IdProveedor = p.IdProveedor,
                CodigoProveedor = p.CodigoProveedor,
                Razon_social = p.Razon_social,
                Nombre_comercial = p.Nombre_comercial,
                RFC = p.RFC,
                Banco = p.Banco,
                Numero_cuenta = p.Numero_cuenta,
                Tipo_documento = p.Tipo_documento,
                Num_documento = p.Num_documento,
                Dias_credito = p.Dias_credito,
                Monto_credito = p.Monto_credito,
                Direccion = p.Direccion,
                Email = p.Email,
                Telefono = p.Telefono,
                Descuento = p.Descuento,
                Descripcion = p.Descripcion,
                Estatus = p.Estatus
            });
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> SelectProveedores()
        {
            //Obtener lista asincrona
            var proveedores = await _context.Proveedores.Where(c => c.Estatus == true).ToListAsync();
            //Retornar el objeto instanciado de la clase SelectViewModel
            return proveedores.Select(p => new SelectViewModel
            {
                IdProveedor = p.IdProveedor,
                Nombre_comercial = p.Nombre_comercial
            });
        }

        // GET: api/Proveedores/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Buscar por ID
            var proveedores = await _context.Proveedores.FindAsync(id);

            if (proveedores == null)
            {
                return NotFound();
            }
            //Retornar el objeto instanciado de la clase ProveedorViewModel
            return Ok(new ProveedorViewModel
            {
                IdProveedor = proveedores.IdProveedor,
                CodigoProveedor = proveedores.CodigoProveedor,
                Razon_social = proveedores.Razon_social,
                Nombre_comercial = proveedores.Nombre_comercial,
                RFC = proveedores.RFC,
                Banco = proveedores.Banco,
                Numero_cuenta = proveedores.Numero_cuenta,
                Tipo_documento = proveedores.Tipo_documento,
                Num_documento = proveedores.Num_documento,
                Dias_credito = proveedores.Dias_credito,
                Monto_credito = proveedores.Monto_credito,
                Direccion = proveedores.Direccion,
                Email = proveedores.Email,
                Telefono = proveedores.Telefono,
                Descuento = proveedores.Descuento,
                Descripcion = proveedores.Descripcion,
                Estatus = proveedores.Estatus
            });
        }

        // PUT: api/Proveedores/Actualizar/5
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IdProveedor < 0)
            {
                return BadRequest();
            }
            //Consultar que el Proveedor exista en BD
            var proveedor = await _context.Proveedores.FirstOrDefaultAsync(c => c.IdProveedor == model.IdProveedor);

            //Validar si existe el Proveedor
            if (proveedor == null)
            {
                return NotFound();
            }
            //Si el Proveedor existe pasarle los valores al objeto
            proveedor.CodigoProveedor = model.CodigoProveedor;
            proveedor.Razon_social = model.Razon_social;
            proveedor.Nombre_comercial = model.Nombre_comercial;
            proveedor.RFC = model.RFC;
            proveedor.Banco = model.Banco;
            proveedor.Numero_cuenta = model.Numero_cuenta;
            proveedor.Tipo_documento = model.Tipo_documento;
            proveedor.Num_documento = model.Num_documento;
            proveedor.Dias_credito = model.Dias_credito;
            proveedor.Monto_credito = model.Monto_credito;
            proveedor.Direccion = model.Direccion;
            proveedor.Email = model.Email;
            proveedor.Telefono = model.Telefono;
            proveedor.Descuento = model.Descuento;
            proveedor.Descripcion = model.Descripcion;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // POST: api/Proveedores/Agregar
        [HttpPost("[action]")]
        public async Task<IActionResult> Agregar([FromBody] AgregarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Crear instancia de Proveedores y pasarle los del model al objeto
            Proveedores proveedores = new Proveedores
            {
                CodigoProveedor = model.CodigoProveedor,
                Razon_social = model.Razon_social,
                Nombre_comercial = model.Nombre_comercial,
                RFC = model.RFC,
                Banco = model.Banco,
                Numero_cuenta = model.Numero_cuenta,
                Tipo_documento = model.Tipo_documento,
                Num_documento = model.Num_documento,
                Dias_credito = model.Dias_credito,
                Monto_credito = model.Monto_credito,
                Direccion = model.Direccion,
                Email = model.Email,
                Telefono = model.Telefono,
                Descuento = model.Descuento,
                Descripcion = model.Descripcion,
                Estatus = true
            };
            //Agregar Proveedor desde el context
            _context.Proveedores.Add(proveedores);
            try
            {
                //Guardar cambios
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Proveedores/Desactivar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que el Proveedor exista en BD
            var proveedores = await _context.Proveedores.FirstOrDefaultAsync(c => c.IdProveedor == id);

            //_context.Entry(clientes).State = EntityState.Modified;

            //Validar si existe el Proveedor
            if (proveedores == null)
            {
                return NotFound();
            }
            //Si el Proveedor existe pasarle los valores al objeto
            proveedores.Estatus = false;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // PUT: api/Proveedores/Activar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que el Proveedor exista en BD
            var proveedores = await _context.Proveedores.FirstOrDefaultAsync(c => c.IdProveedor == id);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe el Proveedor
            if (proveedores == null)
            {
                return NotFound();
            }
            //Si el Proveedor existe pasarle los valores al objeto
            proveedores.Estatus = true;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        private bool ProveedoresExists(int id)
        {
            return _context.Proveedores.Any(e => e.IdProveedor == id);
        }
    }
}