﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Ventas;
using op_soft.Web.Models.Ventas.Ventas;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public VentasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Ventas/Get
        [HttpGet("[action]")]
        public async Task<IEnumerable<VentasViewModel>> Get()
        {
            var venta = await _context.Ventas
                .Include(v => v.Usuario)
                .Include(v => v.Cliente)
                .OrderByDescending(v => v.IdVenta)
                .Take(100)
                .ToListAsync();

            return venta.Select(v => new VentasViewModel
            {
                IdVenta = v.IdVenta,
                IdCliente = v.IdCliente,
                Cliente = v.Cliente.Nombre_comercial,
                IdUsuario = v.IdUsuario,
                Usuario = v.Usuario.Nombre,
                Tipo_comprobante = v.Tipo_comprobante,
                Serie_comprobante = v.Serie_comprobante,
                Num_comprobante = v.Num_comprobante,
                Fecha_hora = v.Fecha_hora,
                Impuesto = v.Impuesto,
                Total = v.Total,
                Estado = v.Estado
            });
        }

        // GET: api/Ventas/FilterGet/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<VentasViewModel>> FilterGet([FromRoute] string texto)
        {
            var venta = await _context.Ventas
                .Include(v => v.Usuario)
                .Include(v => v.Cliente)
                .Where(v => v.Num_comprobante.Contains(texto))
                .OrderByDescending(v => v.IdVenta)
                .ToListAsync();

            return venta.Select(v => new VentasViewModel
            {
                IdVenta = v.IdVenta,
                IdCliente = v.IdCliente,
                Cliente = v.Cliente.Nombre_comercial,
                IdUsuario = v.IdUsuario,
                Usuario = v.Usuario.Nombre,
                Tipo_comprobante = v.Tipo_comprobante,
                Serie_comprobante = v.Serie_comprobante,
                Num_comprobante = v.Num_comprobante,
                Fecha_hora = v.Fecha_hora,
                Impuesto = v.Impuesto,
                Total = v.Total,
                Estado = v.Estado
            });
        }

        // GET: api/Ventas/GetDetalles
        [HttpGet("[action]/{idVenta}")]
        public async Task<IEnumerable<DetalleViewModel>> GetDetalles([FromRoute] int idVenta)
        {
            var detalle = await _context.Detalle_ventas
                .Include(a => a.Articulo)
                .Where(d => d.IdVenta == idVenta)
                .ToListAsync();

            return detalle.Select(d => new DetalleViewModel
            {
                IdArticulo = d.IdArticulo,
                Articulo = d.Articulo.Nombre,
                Cantidad = d.Cantidad,
                Precio = d.Precio,
                Descuento = d.Descuento
            });
        }

        // POST: api/Ventas/Post
        [HttpPost("[action]")]
        public async Task<IActionResult> Post([FromBody] AgregarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var fechaHora = DateTime.Now;

            VentasModel venta = new VentasModel
            {
                IdCliente = model.IdCliente,
                IdUsuario = model.IdUsuario,
                Tipo_comprobante = model.Tipo_comprobante,
                Serie_comprobante = model.Serie_comprobante,
                Num_comprobante = model.Num_comprobante,
                Fecha_hora = fechaHora,
                Impuesto = model.Impuesto,
                Total = model.Total,
                Estado = "Aceptado"
            };


            try
            {
                _context.Ventas.Add(venta);
                await _context.SaveChangesAsync();

                var id = venta.IdVenta;
                foreach (var det in model.Detalles)
                {
                    Detalle_ventas detalle = new Detalle_ventas
                    {
                        IdVenta = id,
                        IdArticulo = det.IdArticulo,
                        Cantidad = det.Cantidad,
                        Precio = det.Precio,
                        Descuento = det.Descuento
                    };
                    _context.Detalle_ventas.Add(detalle);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Ventas/CancelarVenta/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> CancelarVenta([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var venta = await _context.Ventas.FirstOrDefaultAsync(v => v.IdVenta == id);

            if (venta == null)
            {
                return NotFound();
            }

            venta.Estado = "Cancelado";

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        private bool VentasModelExists(int id)
        {
            return _context.Ventas.Any(e => e.IdVenta == id);
        }
    }
}