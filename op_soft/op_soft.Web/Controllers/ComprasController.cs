﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Compras;
using op_soft.Web.Models.Compras.Compras;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComprasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ComprasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Compras/Get
        [HttpGet("[action]")]
        public async Task<IEnumerable<ComprasViewModel>> Get()
        {
            //Obtener lista asincrona
            var compras = await _context.Compras
                .Include(c => c.Usuario)
                .Include(c => c.Proveedor)
                .OrderByDescending(c => c.IdCompra)
                .Take(100)
                .ToListAsync();

            //Retornar el objeto instanciado de la clase ComprasViewModel
            return compras.Select(c => new ComprasViewModel
            {
                IdCompra = c.IdCompra,
                IdProveedor = c.IdProveedor,
                Proveedor = c.Proveedor.Nombre_comercial,
                IdUsuario = c.IdUsuario,
                Usuario = c.Usuario.Nombre,
                Tipo_comprobante = c.Tipo_comprobante,
                Serie_comprobante = c.Serie_comprobante,
                Num_comprobante = c.Num_comprobante,
                Fecha_hora = c.Fecha_hora,
                Impuesto = c.Impuesto,
                Total = c.Total,
                Estado = c.Estado
            });
        }

        // GET: api/Compras/FilterGet/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<ComprasViewModel>> FilterGet([FromRoute] string texto)
        {
            var compra = await _context.Compras
                .Include(c => c.Usuario)
                .Include(c => c.Proveedor)
                .Where(c => c.Num_comprobante.Contains(texto))
                .OrderByDescending(c => c.IdCompra)
                .ToListAsync();

            return compra.Select(c => new ComprasViewModel
            {
                IdCompra = c.IdCompra,
                IdProveedor = c.IdProveedor,
                Proveedor = c.Proveedor.Nombre_comercial,
                IdUsuario = c.IdUsuario,
                Usuario = c.Usuario.Nombre,
                Tipo_comprobante = c.Tipo_comprobante,
                Serie_comprobante = c.Serie_comprobante,
                Num_comprobante = c.Num_comprobante,
                Fecha_hora = c.Fecha_hora,
                Impuesto = c.Impuesto,
                Total = c.Total,
                Estado = c.Estado
            });
        }

        // GET: api/Compras/GetDetalles
        [HttpGet("[action]/{idCompra}")]
        public async Task<IEnumerable<DetalleViewModel>> GetDetalles([FromRoute] int idCompra)
        {
            var detalle = await _context.Detalle_compras
                .Include(a => a.Articulos)
                .Where(d => d.IdCompra == idCompra)
                .ToListAsync();

            return detalle.Select(d => new DetalleViewModel
            {
                IdArticulo = d.IdArticulo,
                Articulo = d.Articulos.Nombre,
                Cantidad = d.Cantidad,
                Precio = d.Precio
            });

        }

        // POST: api/Compras/Post
        [HttpPost("[action]")]
        public async Task<IActionResult> Post([FromBody] AgregarViewModel model)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var fechaHora = DateTime.Now;

            ComprasModel compra = new ComprasModel
            {
                IdProveedor = model.IdProveedor,
                IdUsuario = model.IdUsuario,
                Tipo_comprobante = model.Tipo_comprobante,
                Serie_comprobante = model.Serie_comprobante,
                Num_comprobante = model.Num_comprobante,
                Fecha_hora = fechaHora,
                Impuesto = model.Impuesto,
                Total = model.Total,
                Estado = "Aceptado"
            };

            try
            {
                _context.Compras.Add(compra);
                await _context.SaveChangesAsync();

                var id = compra.IdCompra;
                foreach (var det in model.Detalles)
                {
                    Detalle_compras detalle = new Detalle_compras
                    {
                        IdCompra = id,
                        IdArticulo = det.IdArticulo,
                        Cantidad = det.Cantidad,
                        Precio = det.Precio
                    };
                    _context.Detalle_compras.Add(detalle);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Compras/CancelarCompra/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> CancelarCompra([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var compra = await _context.Compras.FirstOrDefaultAsync(c => c.IdCompra == id);

            if (compra == null)
            {
                return NotFound();
            }

            compra.Estado = "Anulado";

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        private bool ComprasModelExists(int id)
        {
            return _context.Compras.Any(e => e.IdCompra == id);
        }
    }
}