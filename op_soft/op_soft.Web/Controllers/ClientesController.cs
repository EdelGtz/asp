﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Ventas;
using op_soft.Web.Models.Ventas.Clientes;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ClientesController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Clientes
        [HttpGet]
        public IEnumerable<Clientes> GetClientes()
        {
            return _context.Clientes;
        }

        // GET: api/Clientes/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<ClienteViewModel>> Listar()
        {
            //Obtener lista asincrona
            var clientes = await _context.Clientes.ToListAsync();
            //Retornar el objeto instanciado de la clase UsuarioViewModel
            return clientes.Select(c => new ClienteViewModel
            {
                IdCliente = c.IdCliente,
                CodigoCliente = c.CodigoCliente,
                Razon_social = c.Razon_social,
                Nombre_comercial = c.Nombre_comercial,
                RFC = c.RFC,
                Banco = c.Banco,
                Numero_cuenta = c.Numero_cuenta,
                Tipo_documento = c.Tipo_documento,
                Num_documento = c.Num_documento,
                Dias_credito = c.Dias_credito,
                Monto_credito = c.Monto_credito,
                Direccion = c.Direccion,
                Email = c.Email,
                Telefono = c.Telefono,
                Descuento = c.Descuento,
                Descripcion = c.Descripcion,
                Estatus = c.Estatus
            });
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> SelectClientes()
        {
            //Obtener lista asincrona
            var proveedores = await _context.Clientes.Where(c => c.Estatus == true).ToListAsync();
            //Retornar el objeto instanciado de la clase SelectViewModel
            return proveedores.Select(c => new SelectViewModel
            {
                IdCliente = c.IdCliente,
                Nombre_comercial = c.Nombre_comercial
            });
        }

        // GET: api/Clientes/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Buscar por ID
            var clientes = await _context.Clientes.FindAsync(id);

            if (clientes == null)
            {
                return NotFound();
            }
            //Retornar el objeto instanciado de la clase ClienteViewModel
            return Ok(new ClienteViewModel
            {
                IdCliente = clientes.IdCliente,
                CodigoCliente = clientes.CodigoCliente,
                Razon_social = clientes.Razon_social,
                Nombre_comercial = clientes.Nombre_comercial,
                RFC = clientes.RFC,
                Banco = clientes.Banco,
                Numero_cuenta = clientes.Numero_cuenta,
                Tipo_documento = clientes.Tipo_documento,
                Num_documento = clientes.Num_documento,
                Dias_credito = clientes.Dias_credito,
                Monto_credito = clientes.Monto_credito,
                Direccion = clientes.Direccion,
                Email = clientes.Email,
                Telefono = clientes.Telefono,
                Descuento = clientes.Descuento,
                Descripcion = clientes.Descripcion,
                Estatus = clientes.Estatus
            });
        }

        // PUT: api/Clientes/Actualizar/5
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IdCliente < 0)
            {
                return BadRequest();
            }
            //Consultar que el cliente exista en BD
            var cliente = await _context.Clientes.FirstOrDefaultAsync(c => c.IdCliente == model.IdCliente);
            
            //Validar si existe el cliente
            if (cliente == null)
            {
                return NotFound();
            }
            //Si el cliente existe pasarle los valores al objeto
            cliente.CodigoCliente = model.CodigoCliente;
            cliente.Razon_social = model.Razon_social;
            cliente.Nombre_comercial = model.Nombre_comercial;
            cliente.RFC = model.RFC;
            cliente.Banco = model.Banco;
            cliente.Numero_cuenta = model.Numero_cuenta;
            cliente.Tipo_documento = model.Tipo_documento;
            cliente.Num_documento = model.Num_documento;
            cliente.Dias_credito = model.Dias_credito;
            cliente.Monto_credito = model.Monto_credito;
            cliente.Direccion = model.Direccion;
            cliente.Email = model.Email;
            cliente.Telefono = model.Telefono;
            cliente.Descuento = model.Descuento;
            cliente.Descripcion = model.Descripcion;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // POST: api/Clientes/Agregar
        [HttpPost("[action]")]
        public async Task<IActionResult> Agregar([FromBody] AgregarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Crear instancia de Clientes y pasarle los del model al objeto
            Clientes clientes = new Clientes
            {
                CodigoCliente = model.CodigoCliente,
                Razon_social = model.Razon_social,
                Nombre_comercial = model.Nombre_comercial,
                RFC = model.RFC,
                Banco = model.Banco,
                Numero_cuenta = model.Numero_cuenta,
                Tipo_documento = model.Tipo_documento,
                Num_documento = model.Num_documento,
                Dias_credito = model.Dias_credito,
                Monto_credito = model.Monto_credito,
                Direccion = model.Direccion,
                Email = model.Email,
                Telefono = model.Telefono,
                Descuento = model.Descuento,
                Descripcion = model.Descripcion,
                Estatus = true
            };
            //Agregar cliente desde el context
            _context.Clientes.Add(clientes);
            try
            {
                //Guardar cambios
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Clientes/Desactivar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que el cliente exista en BD
            var clientes = await _context.Clientes.FirstOrDefaultAsync(c => c.IdCliente == id);

            //_context.Entry(clientes).State = EntityState.Modified;

            //Validar si existe el cliente
            if (clientes == null)
            {
                return NotFound();
            }
            //Si el cliente existe pasarle los valores al objeto
            clientes.Estatus = false;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // PUT: api/Clientes/Activar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que el cliente exista en BD
            var clientes = await _context.Clientes.FirstOrDefaultAsync(c => c.IdCliente == id);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe el cliente
            if (clientes == null)
            {
                return NotFound();
            }
            //Si el cliente existe pasarle los valores al objeto
            clientes.Estatus = true;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        private bool ClientesExists(int id)
        {
            return _context.Clientes.Any(e => e.IdCliente == id);
        }
    }
}