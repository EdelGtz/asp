﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Almacen;
using op_soft.Web.Models.Almacen.Categorias;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriasController : Controller
    {
        //Declrar objeto
        private readonly DbContextSistema _context;

        //Constructor recibe parametro context
        public CategoriasController(DbContextSistema context)
        {
            //Iniciar objeto context
            _context = context;
        }

        // GET: api/Categorias/Listar
        [HttpGet("[action]")]
        public async Task <IEnumerable<CategoriaViewModel>> Listar()
        {
            //Obtener lista asincrona
            var categorias = await _context.Categorias.ToListAsync();
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return categorias.Select(c => new CategoriaViewModel
            {
                IdCategoria = c.IdCategoria,
                Nombre = c.Nombre,
                Descripcion = c.Descripcion,
                Estatus = c.Estatus
            });
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> Select()
        {
            //Obtener lista asincrona
            var categorias = await _context.Categorias.Where(c => c.Estatus == true).ToListAsync();
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return categorias.Select(c => new SelectViewModel
            {
                IdCategoria = c.IdCategoria,
                Nombre = c.Nombre
            });
        }

        // GET: api/Categorias/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Buscar por ID
            var categorias = await _context.Categorias.FindAsync(id);

            if (categorias == null)
            {
                return NotFound();
            }
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return Ok(new CategoriaViewModel
            {
                IdCategoria = categorias.IdCategoria,
                Nombre = categorias.Nombre,
                Descripcion = categorias.Descripcion,
                Estatus = categorias.Estatus
            });
        }

        // PUT: api/Categorias/Actualizar/5
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] CategoriaViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IdCategoria < 0)
            {
                return BadRequest();
            }
            //Consultar que la categoria exista en BD
            var categorias = await _context.Categorias.FirstOrDefaultAsync(c => c.IdCategoria == model.IdCategoria);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe la categoria
            if (categorias == null)
            {
                return NotFound();
            }
            //Si la categoria existe pasarle los valores al objeto
            categorias.Nombre = model.Nombre;
            categorias.Descripcion = model.Descripcion;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // POST: api/Categorias/Agregar
        [HttpPost("[action]")]
        public async Task<IActionResult> Agregar([FromBody] AgregarViewModel model)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Crear instancia de Categorias y pasarle los del model al objeto
            Categorias categorias = new Categorias
            {
                Nombre = model.Nombre,
                Descripcion = model.Descripcion,
                Estatus = true
            };
            //Agregar categoria desde el context
            _context.Categorias.Add(categorias);
            try
            {
                //Guardar cambios
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                return BadRequest();
            }

            return Ok();
        }

        // DELETE: api/Categorias/Eliminar/5
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categorias = await _context.Categorias.FindAsync(id);
            if (categorias == null)
            {
                return NotFound();
            }

            _context.Categorias.Remove(categorias);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                return BadRequest();
            }
            
            return Ok(categorias);
        }

        // PUT: api/Categorias/Desactivar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que la categoria exista en BD
            var categorias = await _context.Categorias.FirstOrDefaultAsync(c => c.IdCategoria == id);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe la categoria
            if (categorias == null)
            {
                return NotFound();
            }
            //Si la categoria existe pasarle los valores al objeto
            categorias.Estatus = false;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        // PUT: api/Categorias/Activar/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            //Realizar validaciones del Data Annotation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                return BadRequest();
            }
            //Consultar que la categoria exista en BD
            var categorias = await _context.Categorias.FirstOrDefaultAsync(c => c.IdCategoria == id);

            //_context.Entry(categorias).State = EntityState.Modified;

            //Validar si existe la categoria
            if (categorias == null)
            {
                return NotFound();
            }
            //Si la categoria existe pasarle los valores al objeto
            categorias.Estatus = true;

            try
            {
                //Guardar los cambios relizados
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar exception
                return BadRequest();
            }
            //Si todo sale bien retornar OK
            return Ok();
        }

        private bool CategoriasExists(int id)
        {
            return _context.Categorias.Any(e => e.IdCategoria == id);
        }
    }
}