﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using op_soft.Datos;
using op_soft.Entidades.Usuarios;
using op_soft.Web.Models.Usuarios.Rol;

namespace op_soft.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public RolesController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Rols
        [HttpGet]
        public IEnumerable<Rol> GetRol()
        {
            return _context.Roles;
        }

        // GET: api/Categorias/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<RolViewModel>> Listar()
        {
            //Obtener lista asincrona
            var roles = await _context.Roles.ToListAsync();
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return roles.Select(r => new RolViewModel
            {
                IdRol = r.IdRol,
                Nombre = r.Nombre,
                Descripcion = r.Descripcion,
                Estatus = r.Estatus
            });
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> Select()
        {
            //Obtener lista asincrona
            var roles = await _context.Roles.Where(r => r.Estatus == true).ToListAsync();
            //Retornar el objeto instanciado de la clase CategoriaViewModel
            return roles.Select(r => new SelectViewModel
            {
                IdRol = r.IdRol,
                Nombre = r.Nombre
            });
        }

        private bool RolExists(int id)
        {
            return _context.Roles.Any(e => e.IdRol == id);
        }
    }
}