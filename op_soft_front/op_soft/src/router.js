import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Categorias from './components/Categorias.vue'
import Articulos from './components/Articulos.vue'
import Roles from './components/Roles.vue'
import Usuarios from './components/Usuarios.vue'
import Ventas from './components/Ventas.vue'
import Clientes from './components/Clientes.vue'
import Proveedores from './components/Proveedores.vue'
import Compras from './components/Compras.vue'
import Login from './components/Login.vue'
import store from './store'

Vue.use(Router)

//export default new Router({
var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        administrador: true,
        almacen: true,
        vendedor: true
      }
    },
    {
      path: '/categorias',
      name: 'categorias',
      component: Categorias,
      meta: {
        administrador: true,
        almacen: true
      }
    },
    {
      path: '/articulos',
      name: 'articulos',
      component: Articulos,
      meta: {
        administrador: true,
        almacen: true
      }
    },
    {
      path: '/compras',
      name: 'compras',
      component: Compras,
      meta: {
        administrador: true,
        almacen: true
      }
    },
    {
      path: '/roles',
      name: 'roles',
      component: Roles,
      meta: {
        administrador: true
      }
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: Usuarios,
      meta: {
        administrador: true
      }
    },
    {
      path: '/ventas',
      name: 'ventas',
      component: Ventas,
      meta: {
        administrador: true,
        vendedor: true
      }
    },
    {
      path: '/clientes',
      name: 'clientes',
      component: Clientes,
      meta: {
        administrador: true,
        vendedor: true
      }
    },
    {
      path: '/proveedores',
      name: 'proveedores',
      component: Proveedores,
      meta: {
        administrador: true,
        almacen: true,
        vendedor: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        libre: true
      }
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)){
    next()
  } else if (store.state.usuario && store.state.usuario.Rol== 'Administrador'){
    if (to.matched.some(record => record.meta.administrador)){
      next()
    }
  }else if (store.state.usuario && store.state.usuario.Rol== 'Almacen'){
    if (to.matched.some(record => record.meta.almacen)){
      next()
    }
  }else if (store.state.usuario && store.state.usuario.Rol== 'Vendedor'){
    if (to.matched.some(record => record.meta.vendedor)){
      next()
    }
  } else{
    next({
      name: 'login'
    })
  }
})

export default router
