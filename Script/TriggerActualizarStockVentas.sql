CREATE TRIGGER ActualizarStock_Venta
   ON Detalle_ventas
   FOR INSERT
   AS
   UPDATE a SET a.Stock=a.Stock-d.Cantidad
   FROM Articulos AS a INNER JOIN
   INSERTED AS d ON d.IdArticulo=a.IdArticulo